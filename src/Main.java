import javax.management.AttributeNotFoundException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

//        Урок 8. Перегрузка методов.
//        Цель задания:
//        Совершенствование работы с методами в java, знакомство с функционалом
//        переопределения методов
//        Задание:
//        1.


//        Реализуйте методы, max(x,y)
//        -
//                для целых, вещественных чисел, и для строк (в
//        случае строк
//        -
//                возвращает самую длинную)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!");
        System.out.println(max(3,4));
        System.out.println(max(4.3,55.4));
        System.out.println(max("adsfdf","asf"));

//        2.
//        Реализуйте методы and(boolean x, boolean y), and(boolean x, int y), and(int x,
//        boolean y), который будет возвращать логическое И. Целые чис
//        ла равные 0
//        трактовать как false, остальные true.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!");
        System.out.println(ili(true,false));
        System.out.println(ili(true,10));
        System.out.println(ili(0,false));
//        3.
//        Реализуйте методы join(String s1, String s2), join(String s1, String s2, String s3),  ....
//        join(String s1, String s2, String s3, String s4)
//                -
//                которые склеивают строки

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!");
        System.out.println(join("aaa","bbb"));
        System.out.println(join("aaa","bbb","ccc"));
        System.out.println(join("aaa","bbb","ccc","ddd"));
//        4.
//        Реализуйте методы merge(int[] array1, int
//[] array2), merge(int[] array1, int[]
//        array2,int[] array3), merge(int[] array1, int[] array2, int[] array3, int[] array4)
//        -
//                который возвращает новый массив, в котором он соединяет все предыдущие
//                (было 3 массива по 10 элементов, станет массив с 20 элементам
//                        и)

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!");
        int[] array1=new int[]{1,2,3,4};
        int[] array2=new int[]{1,2,3,4};

        merge(array1,array2);
        int[] array3=new int[]{1,2,3,4};
        merge(array1,array2,array3);
        int[] array4=new int[]{4,5,6,7,8};

        merge(array1,array2,array3,array4);

//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                вс
//        е технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов

    }



    public static int max(int x, int y){
        if (x>=y) return x;
        else return y;
    }
    public static double max(double x, double y){
        if (x>=y) return x;
        else return y;
    }
    public static String max(String x, String y){
        if (x.length()>=y.length()) return x;
        else return y;
    }
////////////////////////////////
    public static boolean ili( boolean x, boolean y) {
        return (x|y);
    }

    public static boolean ili( boolean x, int y) {
        if (y>0) return x;
        else return false;
    }
    public static boolean ili( int x, boolean y) {
        if (x>0) return y;
        else return false;
    }
    ///////////////////////////////////////////

    public static String join(String s1, String s2){
        return s1+s2;
    }

    public static String join(String s1, String s2, String s3){
        return s1+s2+s3;
    }

    public static String join(String s1, String s2, String s3, String s4){
        return s1+s2+s3+s4;
    }
    //////////////////////////////////

    public static int[] merge(int[] array1, int[]array2){
        int [] res = new int[array1.length+array2.length];
        System.arraycopy(array1,0,res,0,array1.length);
        System.arraycopy(array2,0,res,array1.length,array2.length);
        System.out.println("----------------------");
        for (int i=0; i<res.length;i++)
            System.out.println(res[i]);
        System.out.println("----------------------");
        return res;
    }

    public static int[] merge(int[] array1, int[]array2,int[]array3){
        int [] res = new int[array1.length+array2.length+array3.length];
        System.arraycopy(array1,0,res,0,array1.length);
        System.arraycopy(array2,0,res,array1.length,array2.length);
        System.arraycopy(array3,0,res,array1.length+array2.length,array3.length);
        System.out.println("----------------------");
        for (int i=0; i<res.length;i++)
            System.out.println(res[i]);
        System.out.println("----------------------");
        return res;
    }

    public static int[] merge(int[] array1, int[]array2,int[]array3,int[]array4){
        int [] res = new int[array1.length+array2.length+array3.length+array4.length];
        System.arraycopy(array1,0,res,0,array1.length);
        System.arraycopy(array2,0,res,array1.length,array2.length);
        System.arraycopy(array3,0,res,array1.length+array2.length,array3.length);
        System.arraycopy(array4,0,res,array1.length+array2.length+array3.length,array4.length);
        System.out.println("----------------------");
        for (int i=0; i<res.length;i++)
            System.out.println(res[i]);
        System.out.println("----------------------");
        return res;
    }
}